// [SECTION] MongoDB Aggregation
    /*
        - to generate and perform operations to create filtered results that helps us analyze the data

    using aggregate method:
        - the "$match" is used to pass the documents that meet the specified condition / condition to the next stage or aggregation process

        Syntax:
            db.collectionName.aggregate({{$match: {field:value}}})
    */

        db.fruits.aggregate([{$match: {onSale: true}}]);

    /*
        - The group is used to group the elements together and field-value the data from the grouped element.

        Syntax:
        {$group: _id: "fieldSetGroup"}
    */
        // returns only the id
        db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id"}}
        ]);

        // returns id and total fruits in each id
        db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", totalFruits: {$sum:"$stock"}}}
        ]);

        // Mini activity:
        // First get all the fruits that are color Yellow then grouped them into their respective supplier and ther available stocks.
        db.fruits.aggregate([
            {$match: {color: "Yellow"}},
            {$group: {_id: "$supplier_id", totalFruits: {$sum:"$stock"}}}
        ]);

    // Field Projection with Aggregation
        /*
            - $project can be used when aggregation data to include /  exclude from the returned result

            Syntax: 
                {$project: {field: 0/1}}


        */

            db.fruits.aggregate([
                {$match: {onSale: true}},
                {$group: {_id: "$supplier_id", totalFruits: {$sum:"$stock"}}},
                {$project: {_id:0}}
            ]);

    // Sorting aggregated results
        // $sort can be used to change the order of the aggregated result
        // Value in sort: 1 = lowest to highest, -1 = highest to lowest

        db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", totalFruits: {$sum:"$stock"}}},
            {$sort: {totalFruits:1}}
        ]);

    // aggregating results based on array fields
        /*
            the $unwind deconstructs an array field from a collection / field with an array value to output a result
        */

            db.fruits.aggregate([
                {$unwind: "$origin"},
                {$group: {_id: "$origin", fruits: {$sum:1}}} // fruits: {$sum:1} display kng ilang klase ng fruits galing each origin
            ]);
        
    // Other Aggregate Stages
        // - counts all yellow fruits. bilangin nya ilan ung match
            db.fruits.aggregate([
                {$match: {color: "Yellow"}},
                {$count: "Yellow Fruits"}
            ])

        // $avg gets the average value of the stock
            db.fruits.aggregate([
                {$match: {color: "Yellow"}},
                {$group: {_id: "$color",
                avgYellow: {$avg:"$stock"}}}
            ]);

        // $min && $max
                db.fruits.aggregate([
                    {$match: {color: "Yellow"}},
                    {$group: {_id: "$color",
                    lowestStock: {$min:"$stock"}}}
                ]);

                db.fruits.aggregate([
                    {$match: {color: "Yellow"}},
                    {$group: {_id: "$color",
                    highestStock: {$max:"$stock"}}}
                ]);